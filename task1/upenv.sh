!/bin/bash

#docker rm -f `docker ps -aq -f name=myproject_*`
set -a
source mysql.env
cat ${COMPOSE_CONFIG} | envsubst | docker-compose -f - -p "myproject" up -d