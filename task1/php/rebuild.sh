#!/bin/bash	

echo Stopping container...
docker stop $(docker ps -a -q --filter ancestor=$1 --format="{{.ID}}")
echo Building container...
docker build -t $1 .
echo Running container...
docker run -t -p 80:80 --rm $1