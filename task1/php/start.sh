#!/bin/bash

echo 'Building container...'
docker build -t $1 .
echo 'Running container...'
docker run -t -p 80:80 --rm $1