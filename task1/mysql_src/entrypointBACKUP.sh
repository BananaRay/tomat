#!/bin/bash
set -e

[[ $DEBUG == true ]] && set -x


DB_REMOTE_ROOT_NAME=${DB_REMOTE_ROOT_NAME:-}
DB_REMOTE_ROOT_PASS=${DB_REMOTE_ROOT_PASS:-}
DB_REMOTE_ROOT_HOST=${DB_REMOTE_ROOT_HOST:-"172.17.0.1"}

MYSQL_PASSWORD=magento25
#MYSQL_LOG_DIR=/var/log/mysql
#MYSQL_DATA_DIR=/var/lib/mysql
#MYSQL_RUN_DIR=/run/mysqld
MYSQL_DATABASE=mydb
MYSQL_CHARSET=${MYSQL_CHARSET:-"utf8"}
MYSQL_COLLATION=${MYSQL_COLLATION:-"utf8_unicode_ci"}


create_data_dir() {
echo Creating ${MYSQL_DATA_DIR}
#rm -rf ${MYSQL_DATA_DIR}
mkdir -p ${MYSQL_DATA_DIR}
chmod -R 0700 ${MYSQL_DATA_DIR}
chown -R ${MYSQL_USER}:${MYSQL_USER} ${MYSQL_DATA_DIR}
#echo "deleting data_dir"
#rm -rf ${MYSQL_DATA_DIR}/*
}

create_run_dir() {
echo Creating ${MYSQL_RUN_DIR}
mkdir -p ${MYSQL_RUN_DIR}
chmod -R 0755 ${MYSQL_RUN_DIR}
chown -R ${MYSQL_USER}:root ${MYSQL_RUN_DIR}
rm -rf ${MYSQL_RUN_DIR}/mysqld.sock.lock
}

create_log_dir() {
echo Creating ${MYSQL_LOG_DIR}
mkdir -p ${MYSQL_LOG_DIR}
chmod -R 0755 ${MYSQL_LOG_DIR}
chown -R ${MYSQL_USER}:${MYSQL_USER} ${MYSQL_LOG_DIR}
}

listen() {
sed -e "s/^bind-address\(.*\)=.*/bind-address = $1/" -i /etc/mysql/mysql.conf.d/mysqld.cnf
}

remove_debian_systen_maint_password() {
  #
  # the default password for the debian-sys-maint user is randomly generated
  # during the installation of the mysql-server package.
  #
  # Due to the nature of docker we blank out the password such that the maintenance
  # user can login without a password.
  #
  sed 's/password = .*/password = /g' -i /etc/mysql/debian.cnf
}


initialize_mysql_database() {
if [ ! -d ${MYSQL_DATA_DIR}/mysql ]; then
  echo "Installing database23232.."

  echo $MYSQL_DATA_DIR
  chmod -R 777 ${MYSQL_DATA_DIR}/
  echo "initialize"
  mysqld --initialize-insecure --user=mysql --explicit_defaults_for_timestamp
  echo "initialize done"

  echo "Starting MySQL server..."
    /usr/bin/mysqld_safe >/dev/null 2>&1 &

  timeout=30
  echo -n "Waiting for database server to accept connections"
  
  while ! /usr/bin/mysqladmin -u root status >/dev/null 2>&1
  do
    timeout=$(($timeout - 1))
    if [ $timeout -eq 0 ]; then
      echo -e "\nCould not connect to database server. Aborting..."
      exit 1
    fi
    echo -n "."
    sleep 1
  done
  
  echo -e "\nCreating debian-sys-maint user..."
  mysql -uroot -e "CREATE USER 'debian-sys-maint'@'localhost' IDENTIFIED BY '';"
  echo "User created"
  mysql -uroot -e "GRANT ALL PRIVILEGES on *.* TO 'debian-sys-maint'@'localhost' IDENTIFIED BY '' WITH GRANT OPTION;"
  echo "PRIVILEGES granted"
  /usr/bin/mysqladmin --defaults-file=/etc/mysql/debian.cnf shutdown
  echo "mysqladmin shutdowned"
fi
}

create_users_and_databases() {
  # create new user / database
    echo "CUAD mysqld_safe exec"
    /usr/bin/mysqld_safe >/dev/null 2>&1 &
        # wait for mysql server to start (max 30 seconds)
    timeout=30
    while ! /usr/bin/mysqladmin -u root status >/dev/null 2>&1
    do
      timeout=$(($timeout - 1))
      if [ $timeout -eq 0 ]; then
        echo "CUAD Could not connect to mysql server. Aborting..."
        exit 1
      fi
      sleep 1
    done

    
     	#DB_USER=$MYSQL_USER
        echo "Creating database \`$MYSQL_DATABASE\`..."
        mysql --defaults-file=/etc/mysql/debian.cnf \
          -e "CREATE DATABASE IF NOT EXISTS \`$MYSQL_DATABASE\` DEFAULT CHARACTER SET \`$MYSQL_CHARSET\` COLLATE \`$MYSQL_COLLATION\`;"

		echo "Creating 123user \"$MYSQL_USER\"..."
    #mysql --defaults-file=/etc/mysql/debian.cnf -e "FLUSH PRIVILEGES;"
    #echo PRIVILEGES flushed
		mysql --defaults-file=/etc/mysql/debian.cnf -e "CREATE USER 'user'@'localhost';"
            echo "Granting access to database \`$MYSQL_DATABASE\` for user \`$MYSQL_USER\`..."
            mysql --defaults-file=/etc/mysql/debian.cnf \
            -e "GRANT ALL PRIVILEGES ON \`$MYSQL_DATABASE\`.* TO '$MYSQL_USER' IDENTIFIED BY '$MYSQL_PASSWORD';"
          
      
    	
    /usr/bin/mysqladmin --defaults-file=/etc/mysql/debian.cnf shutdown
  
}
hostname
create_data_dir
create_run_dir
create_log_dir
listen "127.0.0.1"
remove_debian_systen_maint_password
initialize_mysql_database
create_users_and_databases
listen "0.0.0.0"
#mysqld_safe

